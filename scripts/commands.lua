function HelloCommand()
    io.write("Hello, world from lua function")
end

function AddCommand(x, y)
    result = x + y
    io.write(result, "\n")
end

local state = Engine.Scripting:GetState()
Engine.Commands:Register(state, "Hello", "HelloCommand")
Engine.Commands:Register(state, "Add", "AddCommand")