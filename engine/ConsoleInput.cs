using System;

namespace Hyperion.Engine {
    public class ConsoleInput {
        public ConsoleInput() {

        }

        public int Read() {
            return Console.Read();
        }

        public string ReadLine() {
            return Console.ReadLine();
        }

        public char ReadKey(bool intercept) {
            return Console.ReadKey(intercept).KeyChar;
        }
    }
}