using System;
using Hyperion.Engine.Interfaces;

namespace Hyperion.Engine {
    public class ConsoleLogger : ILogger {
        public void Print(string str, params object[] args) {
            Console.Write(str, args);
        }

        public void PrintLine(string str, params object[] args) {
            Console.WriteLine(str, args);
        }
    }
}