using System;
using System.Text;
using NLua;
using NLua.Exceptions;

namespace Hyperion.Engine {
    public class LuaEngine {
        private Lua _luaState;
        private FileLogger _logger;

        public LuaEngine(Lua luaState = null) {
            _luaState = (luaState != null) ? luaState : new Lua();
            _luaState.State.Encoding = Encoding.UTF8;
            _logger = new FileLogger("./hyperion-log.txt");
        }

        public Lua GetState() {
            return _luaState;
        }

        public void Run(string script) {
            try {
                _luaState.DoFile("./scripts/" + script);
            } catch (LuaScriptException exception) {
                _logger.PrintLine(exception.ToString());
            }
        }

        public void RegisterObject(object obj, string name = "") {
            try {
                if (_luaState != null) {
                    var objName = (name == string.Empty) ? obj.GetType().Name : name;
                    _logger.PrintLine($"Registering Lua Object: {objName}");
                    _luaState[objName] = obj;
                }
                else
                    _logger.PrintLine("Could not register object, lua state is not initialized");
            } catch (Exception exception) {
                _logger.PrintLine(exception.ToString());
            }
        }
    }
}