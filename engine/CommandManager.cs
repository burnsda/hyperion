using System;
using System.Collections.Generic;
using NLua;

namespace Hyperion.Engine {
    public class CommandManager {
        private Dictionary<string, LuaFunction> _commands;

        public CommandManager() {
            _commands = new Dictionary<string, LuaFunction>();
        }

        public void Register(Lua luaState, string commandName, string commandFunction) {
            if (!_commands.ContainsKey(commandName)) {
                var function = luaState[commandFunction] as LuaFunction;
                _commands.Add(commandName, function);
            }                
        }

        public void Call(string name, params object[] args) {
            if (_commands.ContainsKey(name)) {
                _commands[name].Call(args);
            }
        }
    }
}