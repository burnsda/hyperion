namespace Hyperion.Engine.Interfaces {
    public interface ILogger {
        void Print(string str, params object[] args);
        void PrintLine(string str, params object[] args);   
    }
}