using System;
using System.IO;
using Hyperion.Engine.Interfaces;

namespace Hyperion.Engine {
    public class FileLogger : ILogger {
        private string _fileName = "";

        public FileLogger(string fileName) {
            _fileName = fileName;
        }

        public void Print(string str, params object[] args) {
            using (var f = new StreamWriter(_fileName, true)) {
                f.Write(string.Format(str, args));
            }
        }

        public void PrintLine(string str, params object[] args) {
            using (var f = new StreamWriter(_fileName, true)) {
                var content = $"[{Convert.ToString(DateTime.Now)}]: {string.Format(str, args)}";
                f.WriteLine(content);
            }
        }
    }
}