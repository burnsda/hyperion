using System;
using Hyperion.Engine;

namespace Hyperion.Game {
    public class Game {
        private bool _isRunning = false;
        private string _title = "";
        private FileLogger _fileLogger;
        private ConsoleLogger _consoleLogger;
        private ConsoleInput _consoleInput;
        private LuaEngine _lua;
        private CommandManager _commandManager;

        public Game(string title) {
            _title = title;
        }

        public void Run() {
            if (_title != String.Empty)
                Console.Title = _title;

            while (_isRunning) {
                Update();
            }
        }

        public void Initialize() {
            _fileLogger = new FileLogger("./hyperion-log.txt");
            _consoleLogger = new ConsoleLogger();
            _consoleInput = new ConsoleInput();
            _lua = new LuaEngine();
            _commandManager = new CommandManager();

            _lua.RegisterObject(new {
                Console = new {
                    Logger = _consoleLogger,
                    Input = _consoleInput
                },
                Logger = _fileLogger,
                Scripting = _lua,
                Commands = _commandManager
            }, "Engine");

            _lua.Run("game.lua");
            _isRunning = true;
        }

        public void Update() {
           
        }

        public bool IsRunning() {
            return _isRunning;
        }
    }
}