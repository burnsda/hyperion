﻿using Hyperion.Game;

namespace Hyperion {
    public class Program {
        static void Main(string[] args) {
            Game.Game game = new Game.Game("Hyperion");
            
            game.Initialize();
            game.Run();
        }
    }
}
